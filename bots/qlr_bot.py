import numpy as np
import random

from utils.state import State


class QlrBot:
    def __init__(self, name, alpha=1, gamma=1, eps=.1):
        self.name = name

        self.alpha = alpha
        self.gamma = gamma
        self.eps = eps

        self.q_table = dict()

        self.total_reward = 0

    def q(self, state, env, action=None):
        if state not in self.q_table:
            self.q_table[state] = np.zeros(env.action_space.n)

        if action is None:
            return self.q_table[state]

        return self.q_table[state][action]

    def choose_action(self, env, state, ignore_eps=False):
        if random.uniform(0, 1) < self.eps and not ignore_eps:
            return env.action_space.sample()
        else:
            return np.argmax(self.q(state, env))

    def make_action(self, env, is_environmental=False, is_learning=True):
        state = State(env.get_current_state())

        action = self.choose_action(env, state)

        if is_environmental is False:
            observation, reward, done, info = env.step(action)
            self.total_reward += reward
        else:
            observation, reward, done = env.boot_move(action)
            info = None

        if is_learning is True:
            next_state = State(observation)

            self.q(state, env)[action] = self.q(state, env, action) + \
                                         self.alpha * (
                                                 reward + self.gamma * np.max(self.q(next_state, env)) -
                                                 self.q(state, env, action)
                                         )

        return observation, reward, done, info

    def save_model(self, filename):
        np.save(f'{filename}.npy', self.q_table)

    def load_model(self, filename):
        try:
            self.q_table = np.load(f'{filename}.npy').item()
        except IOError:
            return 'File not exist. Starting from scratch.'

        return 'Q table read from file properly. Starting from saved point.'
