class State:
    def __init__(self, game_state):
        self.game_state = game_state

    def __eq__(self, other):
        return self.game_state == other

    def __hash__(self):
        return hash(';'.join(self.game_state))

    def __str__(self):
        return ';'.join(self.game_state)
