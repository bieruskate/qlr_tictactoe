from tkinter import Tk

from engine.tic_tac import TicTacToe

if __name__ == '__main__':
    BOARD_SIZE = 3

    tk = Tk()
    TicTacToe(tk, BOARD_SIZE)

    tk.mainloop()
