from tkinter import Tk

import gym
from gym import spaces

from engine.tic_tac import TicTacToe
from bots.qlr_bot import QlrBot
import random


class TicTacEnv(gym.Env):
    _seed = 0
    metadata = {'render.modes': ['human']}

    board_size = 3

    def __init__(self):
        self.action_space = spaces.Discrete(self.board_size ** 2)
        self.observation_space = spaces.Discrete(self.board_size ** 2)

        self.tk = Tk()
        self.game = TicTacToe(self.tk, self.board_size)

        self.player_sign = 'X'
        self.env_sign = 'O'
        self.state = None
        self.q_bot = QlrBot('Env', eps=0.2)

        self.random_opponent = True

    def get_current_state(self):
        return self.state

    def step(self, action):
        done = False
        reward = 0
        info = {}

        board = self.game.get_board_state()
        proposed = board[action]

        if proposed != '':
            info['error'] = 'Field already taken'
            done = True
            reward = -1
        else:
            self.game.make_move_by_index(action, self.player_sign)
            self.state = self.game.get_board_state()

        if self.game.check_winner() is None:
            possible_moves = self.game.get_possible_moves()
            if len(possible_moves) > 0:
                if self.random_opponent:
                    self.game.make_move_by_index(random.choice(self.game.get_possible_moves()), self.env_sign)
                    self.state = self.game.get_board_state()
                else:
                    _, _, done, _ = self.q_bot.make_action(self, is_environmental=True)

        winner = self.game.get_winner()

        if winner is not None:
            done = True
            if winner == self.player_sign:
                reward = 1
            else:
                reward = -1
        elif self.game.actions_taken == self.board_size ** 2:
            done = True

        return self.state, reward, done, info

    def boot_move(self, action):
        reward = 0
        board = self.game.get_board_state()
        proposed = board[action]
        done = False

        if proposed != '':
            reward = -1
            done = True
        else:
            self.game.make_move_by_index(action, self.env_sign)
            self.state = self.game.get_board_state()

        winner = self.game.get_winner()

        if winner is not None:
            done = True
            if winner == self.player_sign:
                reward = 1
            else:
                reward = -1

        return self.state, reward, done

    def reset(self):
        self.state = ['' for _ in range(self.board_size ** 2)]

        self.game = TicTacToe(self.tk, self.board_size)
        return self.state

    def render(self, mode='human', close=False):
        self.tk.update()
