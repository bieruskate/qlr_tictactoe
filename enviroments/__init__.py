from gym.envs.registration import register

register(
    id='tictacen-v0',
    entry_point='enviroments.tic_tac_env:TicTacEnv',
)