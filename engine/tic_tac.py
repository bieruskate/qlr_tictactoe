from functools import partial
from tkinter import Button, DISABLED
import numpy as np


class TicTacToe:
    def __init__(self, tk, board_size):
        self.tk = tk
        self.tk.title('Tic Tac Toe')

        self.board_size = board_size
        self.opponent_turn = False

        self.buttons = self._init_board()
        self.actions_taken = 0

    def _init_board(self):
        buttons = []

        for row in range(self.board_size):
            buttons.append([])

            for column in range(self.board_size):
                button = Button(self.tk, width=2, font='Arial 40', bg='white')
                button['command'] = partial(self._handle_click, button)
                button.grid(row=row, column=column)

                buttons[row].append(button)

        return np.array(buttons)

    def _handle_click(self, button):
        if button['text'] == '':
            button['text'] = 'X' if self.opponent_turn else 'O'
            self.opponent_turn = not self.opponent_turn
            self.actions_taken += 1

        self.check_winner()

    def check_winner(self):
        checks = [self._check_horizontal(), self._check_vertical(), self._check_diagonal()]
        winner = next((w for w in checks if w is not None), None)

        if winner:
            for b in self.buttons.ravel():
                b['state'] = DISABLED
            return winner

        elif self.actions_taken == self.board_size ** 2:
            return ''
        return None

    def _check_horizontal(self):
        return self._check_sequences(self.buttons)

    def _check_vertical(self):
        columns = [self.buttons[:, i] for i in range(self.board_size)]
        return self._check_sequences(columns)

    def _check_diagonal(self):
        diagonals = [np.diag(self.buttons), np.diag(np.fliplr(self.buttons))]
        return self._check_sequences(diagonals)

    def get_possible_moves(self):
        result = []
        for i in range(self.board_size ** 2):
            if self.buttons[int(i / self.board_size)][i % self.board_size]['text'] == '':
                result.append(i)

        return result

    def get_board_state(self):
        result = []
        for i in range(self.board_size ** 2):
            sign = self.buttons[int(i / self.board_size)][i % self.board_size]['text']
            result.append(sign)
        return result

    def make_move_by_index(self, index, sign):
        self.buttons[int(index / self.board_size)][index % self.board_size]['text'] = sign
        self.opponent_turn = not self.opponent_turn

    def get_winner(self):
        return self.check_winner()

    @staticmethod
    def _check_sequences(seq_collection):
        for sequence in seq_collection:
            first_sign = sequence[0]['text']

            if first_sign != '':
                if all([b['text'] == first_sign for b in sequence]):
                    return first_sign
        return None
