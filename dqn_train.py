import gym
import enviroments
from bots.dqn import DQNAgent

import numpy as np


def map_state_no_numeric(state):
    mapping = {'X': 1, 'O': 2, '': 3}
    return [mapping[s] for s in state]


if __name__ == "__main__":
    learn = False

    EPISODES = 10000000
    display_per_episodes = 50

    hl_size = 9

    env = gym.make('tictacen-v0')
    env.board_size = 3
    env.random_opponent = True

    state_size = env.observation_space.n
    action_size = env.action_space.n
    agent = DQNAgent(state_size, action_size, hl_size)

    agent.load(f'saves/dqn_h{hl_size}_save{env.board_size}.h5')

    batch_size = 32

    total_reward = 0
    wins_in_batch = 0

    state = env.reset()
    state = map_state_no_numeric(state)

    if learn:
        print(f'Starting hl:{hl_size}')

        for episode in range(EPISODES):

            while True:
                # env.render()

                state = np.reshape(state, [1, state_size])

                action = agent.act(state)
                next_state, reward, done, info = env.step(action)
                total_reward += reward

                next_state = map_state_no_numeric(next_state)

                next_state = np.reshape(next_state, [1, state_size])
                agent.remember(state, action, reward, next_state, done)
                state = next_state

                if done:
                    state = env.reset()
                    state = map_state_no_numeric(state)
                    if reward == 1:
                        wins_in_batch += 1
                    break

                if len(agent.memory) > batch_size:
                    agent.replay(batch_size)

            if episode % display_per_episodes == 0:
                print(f'Episode {episode + 1}: total reward -> {total_reward}, eps: {agent.epsilon}, '
                      f'memory size: {len(agent.memory)}, wins ratio: {wins_in_batch / display_per_episodes}')

                agent.save(f'saves/dqn_h{hl_size}_save{env.board_size}.h5')
                wins_in_batch = 0

    else:
        games = 1000
        agent.epsilon = 0

        for episode in range(games):

            while True:
                state = np.reshape(state, [1, state_size])

                action = agent.act(state)
                next_state, reward, done, info = env.step(action)
                total_reward += reward

                next_state = map_state_no_numeric(next_state)
                state = next_state

                # env.render()
                # time.sleep(.6)

                if done:
                    state = env.reset()
                    state = map_state_no_numeric(state)
                    if reward == 1:
                        wins_in_batch += 1
                    break

            print(f'Won: {wins_in_batch}/{episode + 1} ({100 * wins_in_batch/(episode +1)}%)')
