import gym
import enviroments

from bots.qlr_bot import QlrBot
from utils.state import State

if __name__ == '__main__':
    learn = False

    N_EPISODES = 100000
    display_per_episodes = 50

    q_bot = QlrBot('Agent', eps=0)

    env = gym.make('tictacen-v0')
    env.board_size = 3
    env.random_opponent = True

    load_info = q_bot.load_model(f'saves/q_save{env.board_size}')
    print(load_info)

    if learn:
        observation = env.reset()
        state = State(observation)

        total_reward = 0
        wins_in_batch = 0

        for episode in range(N_EPISODES):
            # env.render()

            while True:
                observation, reward, done, info = q_bot.make_action(env)
                total_reward += reward

                if done:
                    env.reset()

                    if reward == 1:
                        wins_in_batch += 1
                    break

            if episode % display_per_episodes == 0:
                print(f'Episode {episode + 1}: total reward -> {total_reward}, '
                      f'wins ratio: {wins_in_batch / display_per_episodes}')
                print(f'States found: {len(q_bot.q_table.keys())}')
                q_bot.save_model(f'saves/q_save{env.board_size}')

                wins_in_batch = 0
    else:
        games = 1000

        observation = env.reset()
        state = State(observation)

        wins_in_batch = 0

        env.render()
        for episode in range(games):
            while True:
                action = q_bot.choose_action(env, state, ignore_eps=True)
                observation, reward, done, info = env.step(action)

                state = State(observation)

                # env.render()
                # time.sleep(1)

                if done:
                    env.reset()
                    if reward == 1:
                        wins_in_batch += 1
                    break

            print(f'Won: {wins_in_batch}/{episode + 1} ({100 * wins_in_batch/(episode +1)}%)')
